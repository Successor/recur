#include <iostream>

using namespace std;

int product(int a, int b)
{
	int result = 0;
	if (b < 0)
	{
		a = -a;
		b = -b;
	}
	
	if(b > 0)
	{
		result += a;
		result = result + product(a, b -1);
	}
	return result;
}



int main()
{
	int result = product(-5, 3);
	cout << result << endl;
}
